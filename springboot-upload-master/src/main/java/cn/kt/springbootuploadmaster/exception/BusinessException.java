package cn.kt.springbootuploadmaster.exception;

import cn.kt.springbootuploadmaster.enums.MessageEnum;
import lombok.Data;

/**
 * Created by tao.
 * Date: 2022/6/29 11:26
 * 描述:
 */
@Data
public class BusinessException extends BaseErrorException {

    private static final long serialVersionUID = 2369773524406947262L;

    public BusinessException(MessageEnum messageEnum) {
        super(messageEnum);
    }

    public BusinessException(String error) {
        super.setCode(-1);
        super.setError(error);
    }
}
