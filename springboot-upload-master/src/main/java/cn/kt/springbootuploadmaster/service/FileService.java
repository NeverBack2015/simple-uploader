package cn.kt.springbootuploadmaster.service;

import cn.kt.springbootuploadmaster.domin.FileChunkParam;

/**
 * Created by tao.
 * Date: 2022/6/29 11:22
 * 描述:
 */
public interface FileService {
    /**
     * 上传文件
     * @param param 参数
     * @return
     */
    boolean uploadFile(FileChunkParam param);
}
