package cn.kt.springbootuploadmaster.repository;

import cn.kt.springbootuploadmaster.domin.FileChunkParam;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;
import java.util.Optional;

/**
 * Created by tao.
 * Date: 2022/6/29 10:51
 * 描述:
 */
public interface FileChunkRepository extends JpaRepository<FileChunkParam, Long>, JpaSpecificationExecutor<FileChunkParam> {

    List<FileChunkParam> findByIdentifier(String identifier);
}
