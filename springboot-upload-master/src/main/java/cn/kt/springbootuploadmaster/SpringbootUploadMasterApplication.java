package cn.kt.springbootuploadmaster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootUploadMasterApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootUploadMasterApplication.class, args);
    }

}
